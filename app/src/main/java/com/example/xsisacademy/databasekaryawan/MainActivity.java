package com.example.xsisacademy.databasekaryawan;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.xsisacademy.databasekaryawan.model.Karyawan;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.xsisacademy.databasekaryawan.AppController.db;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.etNama) EditText etNama;
    @BindView(R.id.etID) EditText etID;
    @BindView(R.id.etDivisi) EditText etDivisi;
    @BindView(R.id.etGaji) EditText etGaji;

    Karyawan karyawan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnDaftar)void daftarKaryawan(){
        if (!etNama.getText().toString().isEmpty()
                && !etID.getText().toString().isEmpty()
                && !etDivisi.getText().toString().isEmpty()
                && !etGaji.getText().toString().isEmpty()){
            AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
            builderSingle.setIcon(R.drawable.ic_warning_black_24dp);
            builderSingle.setTitle("Warning");
            builderSingle.setMessage("Data sudah oke?");

            builderSingle.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    karyawan = new Karyawan();
                    karyawan.setNama_karyawan(etNama.getText().toString());
                    karyawan.setId_karyawan(etID.getText().toString());
                    karyawan.setDivisi_karyawan(etDivisi.getText().toString());
                    karyawan.setGaji_karyawan(etGaji.getText().toString());

                    //insert data ke db
                    db.daoAccess().insertAll(karyawan);
                    Toast.makeText(getApplicationContext(), "Berhasil mendaftar", Toast.LENGTH_SHORT).show();
                }
            });
            builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builderSingle.show();

        } else {
            Toast.makeText(this, "Lengkapi field yang kosong", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.btnLihatData) void lihatData(){
        startActivity(new Intent(MainActivity.this, DetailActivity.class));
    }

    @OnClick(R.id.btnDeleteAll) void deleteData(){
        db.daoAccess().deleteAll();

    }
}
