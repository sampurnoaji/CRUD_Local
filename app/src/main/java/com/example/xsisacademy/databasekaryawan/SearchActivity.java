package com.example.xsisacademy.databasekaryawan;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.xsisacademy.databasekaryawan.adapter.KaryawanAdapter;
import com.example.xsisacademy.databasekaryawan.db.KaryawanDatabase;
import com.example.xsisacademy.databasekaryawan.model.Karyawan;

import java.util.ArrayList;
import java.util.List;

import static com.example.xsisacademy.databasekaryawan.AppController.db;

public class SearchActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    String isiItemSearch;
    KaryawanAdapter karyawanAdapter;
    List<Karyawan> karyawanList = new ArrayList<>();

    Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Intent intent = getIntent();
        isiItemSearch = intent.getStringExtra("kata_kunci");
        Toast.makeText(this, ""+ isiItemSearch, Toast.LENGTH_SHORT).show();

        setupView();
        loadData();
        showRecyclerView();
    }

    private void setupView() {
        recyclerView = findViewById(R.id.rvKaryawan);
        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), DetailActivity.class));
                finish();
            }
        });
    }

    private void showRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false));
        karyawanAdapter = new KaryawanAdapter(this, karyawanList);
        recyclerView.setAdapter(karyawanAdapter);
    }

    private void loadData() {
        db = Room.databaseBuilder(this, KaryawanDatabase.class, "karyawan").allowMainThreadQueries().build();
        karyawanList = db.daoAccess().findByName(isiItemSearch);
    }
}
