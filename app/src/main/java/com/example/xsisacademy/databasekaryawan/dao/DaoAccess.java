package com.example.xsisacademy.databasekaryawan.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.xsisacademy.databasekaryawan.model.Karyawan;

import java.util.List;

/**
 * Created by XsisAcademy on 26/06/2019.
 */
@Dao
public interface DaoAccess {
    @Query("SELECT * FROM karyawan")
    List<Karyawan> getAll();

    //example custom query
    @Query("SELECT * FROM karyawan WHERE nama_karyawan LIKE :nama_karyawan")
    List<Karyawan> findByName(String nama_karyawan);

    @Query("SELECT * FROM karyawan WHERE id LIKE :id")
    Karyawan findById(String id);

    @Insert
    void insertAll(Karyawan karyawans);

    @Delete
    public void deleteUsers(Karyawan users);

    @Query("DELETE FROM karyawan")
    void deleteAll();

    @Query("DELETE FROM karyawan WHERE id LIKE :id")
    void deleteData(int id);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void updateUsers(Karyawan karyawan);
}
