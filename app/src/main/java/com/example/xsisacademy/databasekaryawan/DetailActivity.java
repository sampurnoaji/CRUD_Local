package com.example.xsisacademy.databasekaryawan;

import android.app.Activity;
import android.app.SearchManager;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.xsisacademy.databasekaryawan.adapter.KaryawanAdapter;
import com.example.xsisacademy.databasekaryawan.db.KaryawanDatabase;
import com.example.xsisacademy.databasekaryawan.model.Karyawan;

import java.util.ArrayList;
import java.util.List;

import static com.example.xsisacademy.databasekaryawan.AppController.db;

public class DetailActivity extends AppCompatActivity{

    private List<Karyawan> karyawanList = new ArrayList<>();
    private KaryawanAdapter karyawanAdapter;
    private RecyclerView recyclerView;

    private EditText etSearch;
    private ImageView imgSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        setupView();
        loadData();
        showRecyclerView();
//        setAdapter();
    }

    private void setupView() {
        recyclerView = findViewById(R.id.rvKaryawan);

        etSearch = findViewById(R.id.etSearch);
        imgSearch = findViewById(R.id.imgSearch);

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cariNama();
            }
        });

    }

    private void cariNama() {
        if (!etSearch.getText().toString().isEmpty()){
            Toast.makeText(this, "Mencari...", Toast.LENGTH_SHORT).show();

            String kataKunci = etSearch.getText().toString();
            Intent intent = new Intent(this, SearchActivity.class);
            intent.putExtra("kata_kunci", kataKunci);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, "Field masih kosong...", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadData() {
        db = Room.databaseBuilder(this, KaryawanDatabase.class, "karyawan").allowMainThreadQueries().build();
        karyawanList = db.daoAccess().getAll();
    }

    private void showRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false));
        karyawanAdapter = new KaryawanAdapter(this, karyawanList);
        setAdapter(karyawanAdapter);

    }

    private void setAdapter(KaryawanAdapter karyawanAdapter) {
        recyclerView.setAdapter(karyawanAdapter);
    }
}
