package com.example.xsisacademy.databasekaryawan.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.xsisacademy.databasekaryawan.dao.DaoAccess;
import com.example.xsisacademy.databasekaryawan.model.Karyawan;

/**
 * Created by XsisAcademy on 26/06/2019.
 */
@Database(entities = {Karyawan.class}, version = 1)
public abstract class KaryawanDatabase extends RoomDatabase{
    public abstract DaoAccess daoAccess();
}
