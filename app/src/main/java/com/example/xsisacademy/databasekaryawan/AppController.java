package com.example.xsisacademy.databasekaryawan;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.example.xsisacademy.databasekaryawan.db.KaryawanDatabase;

/**
 * Created by XsisAcademy on 26/06/2019.
 * menyiapkan apa saja yg harus disiapkan sebelum aplikasi dijalankan
 * harus didaftarkan ke android manifest
 */

public class AppController extends Application {
    public static KaryawanDatabase db;

    @Override
    public void onCreate() {
        super.onCreate();
        db = Room.databaseBuilder(getApplicationContext(), KaryawanDatabase.class,"karyawan")
                .allowMainThreadQueries()
                .build();
    }
}
