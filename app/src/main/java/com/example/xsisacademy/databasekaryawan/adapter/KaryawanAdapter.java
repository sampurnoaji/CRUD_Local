package com.example.xsisacademy.databasekaryawan.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.text.UnicodeSetSpanner;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xsisacademy.databasekaryawan.DetailActivity;
import com.example.xsisacademy.databasekaryawan.R;
import com.example.xsisacademy.databasekaryawan.UpdateActivity;
import com.example.xsisacademy.databasekaryawan.model.Karyawan;

import java.util.List;

import static com.example.xsisacademy.databasekaryawan.AppController.db;

/**
 * Created by XsisAcademy on 26/06/2019.
 */

public class KaryawanAdapter extends RecyclerView.Adapter<KaryawanAdapter.ViewHolder> {
    private Context context;
    private List<Karyawan> karyawanList;

    public KaryawanAdapter(Context context, List<Karyawan> karyawanList) {
        this.context = context;
        this.karyawanList = karyawanList;
    }

    @NonNull
    @Override
    public KaryawanAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_list_karyawan, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull KaryawanAdapter.ViewHolder holder, int position) {
        final Karyawan karyawan = karyawanList.get(position);
        holder.nama.setText(karyawan.getNama_karyawan());
        holder.id.setText(karyawan.getId_karyawan());
        holder.divisi.setText(karyawan.getDivisi_karyawan());
        holder.gaji.setText(karyawan.getGaji_karyawan());

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
                builderSingle.setIcon(R.drawable.ic_warning_black_24dp);
                builderSingle.setTitle("Warning");
                builderSingle.setMessage("Yakin mau menghapus?");

                builderSingle.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        db.daoAccess().deleteData(karyawan.getId());
                        Intent intent = new Intent(context, DetailActivity.class);
                        context.startActivity(intent);
                        ((Activity)context).finish();
                        Toast.makeText(context, "Deleted...", Toast.LENGTH_SHORT).show();
                    }
                });
                builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builderSingle.show();
            }
        });

        holder.btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            Intent intent = new Intent(context, UpdateActivity.class);
            intent.putExtra("id", karyawan.getId());
            context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return karyawanList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nama, id, divisi, gaji;
        Button btnDelete, btnUpdate;

        public ViewHolder(View itemView) {
            super(itemView);
            nama = itemView.findViewById(R.id.valueNama);
            id = itemView.findViewById(R.id.valueID);
            divisi = itemView.findViewById(R.id.valueDivisi);
            gaji = itemView.findViewById(R.id.valueGaji);

            btnDelete = itemView.findViewById(R.id.btnDelete);
            btnUpdate = itemView.findViewById(R.id.btnUpdate);
        }
    }
}
