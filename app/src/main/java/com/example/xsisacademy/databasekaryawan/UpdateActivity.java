package com.example.xsisacademy.databasekaryawan;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.xsisacademy.databasekaryawan.model.Karyawan;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.xsisacademy.databasekaryawan.AppController.db;

public class UpdateActivity extends AppCompatActivity {

    @BindView(R.id.btnUpdate) Button btnUpdate;

    @BindView(R.id.etNama) EditText etNama;
    @BindView(R.id.etID) EditText etID;
    @BindView(R.id.etDivisi) EditText etDivisi;
    @BindView(R.id.etGaji) EditText etGaji;

    Karyawan karyawan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        ButterKnife.bind(this);
        getData();
    }

    private void getData() {
        Intent intent = getIntent();
        Integer valueId = intent.getIntExtra("id", 0);
        karyawan = db.daoAccess().findById(String.valueOf(valueId));
        mapData(karyawan);
    }

    private void mapData(Karyawan k) {
        etNama.setText(k.getNama_karyawan());
        etID.setText(k.getId_karyawan());
        etDivisi.setText(k.getDivisi_karyawan());
        etGaji.setText(k.getGaji_karyawan());
    }

    @OnClick(R.id.btnUpdate) void updateData(){
        if (!etNama.getText().toString().isEmpty()
                && !etID.getText().toString().isEmpty()
                && !etDivisi.getText().toString().isEmpty()
                && !etGaji.getText().toString().isEmpty()){
            AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
            builderSingle.setIcon(R.drawable.ic_warning_black_24dp);
            builderSingle.setTitle("Warning");
            builderSingle.setMessage("Data sudah oke?");

            builderSingle.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Karyawan karyawanUpdate = new Karyawan();
                    karyawanUpdate.setId(karyawan.getId());
                    karyawanUpdate.setNama_karyawan(etNama.getText().toString());
                    karyawanUpdate.setId_karyawan(etID.getText().toString());
                    karyawanUpdate.setDivisi_karyawan(etDivisi.getText().toString());
                    karyawanUpdate.setGaji_karyawan(etGaji.getText().toString());

                    //insert data ke db
                    db.daoAccess().updateUsers(karyawanUpdate);
                    Toast.makeText(getApplicationContext(), "Berhasil mengupdate data", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), DetailActivity.class));
                    finish();
                }
            });
            builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builderSingle.show();

        } else {
            Toast.makeText(this, "Lengkapi field yang kosong", Toast.LENGTH_SHORT).show();
        }
    }
}
